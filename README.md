# Goal

**Table Schema** [is a](specification https://frictionlessdata.io/specs/table-schema/)

This library provides tools to transform tableSchema specification into:
- spark dataframe StructType
- spark dataset Case Class
- yaml
- liquibase

# Compatible column types

The [types are listed here](./types.yaml)

## Usage: scala structure

The dataset API has several advantages over the dataframe and rdd API: it is
strongly typed. This allows compilation error detection and also great IDE
features such updating fields in the code easily.

However datasets are available only for scala code. Apparently the dataset
slows down computation a bit over the dataframe, but it takes less memory
(better serialization).

1. Create a yaml **table schema** file
1. run `python generateSpark.py --file tests/ts-example1.yaml --package my.spark.package > path/to/MyScalaClass.scala`

Given the [yaml file](./tests/ts-example1.yaml), the `path/to/MyScalaClass.scala` file contains:

``` scala
package my.spark.package

import org.apache.spark.sql.types._
import org.apache.spark.sql.functions.col

object xxxy {
 val defaultNull = new MetadataBuilder().putNull("default").build
 val schema = StructType(StructField("id", StringType , true, defaultNull)::Nil)

 object column{
  val id = col("id")
 }
}

object yyy {
 val defaultNull = new MetadataBuilder().putNull("default").build
 val schema = StructType(StructField("parent", IntegerType , false)::StructField("id", IntegerType , true, defaultNull)::Nil)

 object column{
  val parent = col("parent")
  val id = col("id")
 }
}


case class xxxy(id: String)
case class yyy(parent: Int, id: Int)

```

It is then possible to use those **schema** in your spark code:

``` scala
import io.frama.parisni.spark.dataframe.DFTool

val someDf = sql("select 1 as parent, 2 as id")

// apply structure, verify nulllity and cast columns
val df = DFTool.appySchema(someDf, xxxy.schema)

// produce a dataset
val ds = df.as[xxxy]

ds.select(xxxy.column.parent, xxxy.column.id)
```



# Install
```
pip install -r requirements.txt
pip install .
```

# Tests
```
python -m pytest
```

# Code Style
```
pycodestyle
```



