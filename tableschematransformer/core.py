import logging
import json
import yaml
from tableschema import validate, exceptions
from tableschema import Schema



class TableSchemaTransformer():

    def __init__(self, path: str):
        self.path = path
        self.resources = []
        if path.endswith(".yaml"):
            self.from_yaml()
        with open('types.yaml') as f:
            self.types = yaml.load(f, Loader=yaml.FullLoader)

    def from_yaml(self):
        with open(self.path) as f:
            descriptor = yaml.load(f, Loader=yaml.FullLoader)
        for r in descriptor["resources"]:
            schema = Schema(r['schema'])
            logging.info(r)
            try:
                valid = validate(schema.descriptor)
                self.resources.append({'tableName':r["name"],'tableSchema':schema})
            except exceptions.ValidationError as exception:
                logging.error(exception.errors)

    def generate_spark_source(self, package:str):
        df = self.to_scala_structtype()
        ds = self.to_scala_caseclass()
        typeStr = """package %s
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions.col
import java.sql.Timestamp

%s

%s
""" % (package, df, ds)

        return typeStr

    def to_scala_caseclass(self):
        result = []
        for table in self.resources:
            logging.info(table.get('tableName'))
            a =self.to_scala_casefield(table.get('tableSchema'))
            res = "case class %s(%s)" % (table.get('tableName'), a)
            result.append(res)
        return '\n'.join(str(x) for x in result)

    def to_scala_casefield(self, schema):
        result = []
        logging.info(type(schema.descriptor))
        for field in schema.descriptor.get('fields'):
            typeStr = self.to_scala_type(field.get('type'))
            res = '%s: %s' % (field.get('name'), typeStr)
            result.append(res)
        return  ', '.join(str(x) for x in result)

    def to_scala_structtype(self):
        result = []
        for table in self.resources:
            logging.info(table.get('tableName'))
            a =self.to_scala_structfield(table.get('tableSchema'))
            b =self.to_scala_col(table.get('tableSchema'))
            res = """object %s {
 val defaultNull = new MetadataBuilder().putNull("default").build
 val schema = StructType(%s::Nil)

 object column{
  %s
 }
}
""" % (table.get('tableName'), a, b)
            result.append(res)
        return '\n'.join(str(x) for x in result)
             
    def to_scala_col(self, schema):
        result = []
        for field in schema.descriptor.get('fields'):
            typeStr = self.to_spark_type(field.get('type'))
            res = 'val %s = col("%s")' % (field.get('name'), field.get('name'))
            result.append(res)
        return  '\n'.join(str(x) for x in result)

    def to_scala_structfield(self, schema):
        result = []
        logging.info(type(schema.descriptor))
        for field in schema.descriptor.get('fields'):
            typeStr = self.to_spark_type(field.get('type'))
            requiredStr = ', true, defaultNull' # this adds null values 
            if 'constraints' in field:
                if 'required' in field.get('constraints'):
                    requiredStr = ', false'
            res = 'StructField("%s", %s %s)' % (field.get('name'), typeStr, requiredStr)
            result.append(res)
        return  '::'.join(str(x) for x in result)

    def to_spark_type(self, key:str):
        if key not in self.types:
            raise Exception('%s to scala type not yet implemented' % key)
        return self.types[key]['spark']

    def to_scala_type(self, key:str):
        if key not in self.types:
            raise Exception('%s to scala type not yet implemented' % key)
        return self.types[key]['scala'] 
